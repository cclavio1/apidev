const User = require('./../models/User')
const CryptoJS =  require('crypto-js')
const {createToken,verifyToken,decodeToken} = require('./../auth');
const { json } = require('express/lib/response');
const req = require('express/lib/request');

module.exports.register = async (req,res)=>{
    const {firstName,lastName,email,password}= req.body;
    let newUser = new User({
        firstName:firstName,
        lastName:lastName,
        email:email,
        password:CryptoJS.AES.encrypt(password,process.env.SECRETKEY).toString()
    })
    try{
    await newUser.save().then(result=>{
        res.json({status:"ok",newUser:result})
      
    })}catch(err){
        res.json({status:"error",error:"duplicate email"})
        
    }
}

module.exports.getAllUsers=(req,res)=> User.find().then(result=>res.send(result))

module.exports.login= async(req,res)=>{
    let email = req.body.email;
    let password = req.body.password
    await User.findOne({email:email}).then(result=>{
        if(result===null){
            res.status(511).json({status:"error",error:"email not registered"})
        }else{
            let decryptPass = CryptoJS.AES.decrypt(result.password,process.env.SECRETKEY).toString(CryptoJS.enc.Utf8); 
            if(password==decryptPass){
                let token = createToken(result);
                
                res.json(token)
            }else{
                res.status(511).json({status:"error",error:"Authentication Failed"})
            }
        }
    })
}

module.exports.profile=async(req,res)=>{
    let token = req.headers.authorization
    let userInfo = decodeToken(token)
    User.findById(userInfo.id).then(result=>res.send(result))
}

module.exports.update=async(req,res)=>{
    let token = req.headers.authorization
    let userInfo = decodeToken(token)
    User.findByIdAndUpdate(userInfo.id,{$set:{firstName:"updated"}},{new:true})
    .then(result=>{
        res.json({newValue:result})
    })
}

module.exports.updatePassword=async(req,res)=>{
    let token = req.headers.authorization
    let userInfo = decodeToken(token)
    let newPass = req.body.password;
    let encPass = CryptoJS.AES.encrypt(newPass,process.env.SECRETKEY).toString()
    await User.findByIdAndUpdate(userInfo.id,{$set:{password:encPass}},{new:true})
    .then(result=>{
       result.password= "***"
        res.json({status:"updated",newValue:result})
    })
}

module.exports.giveAdminAccess=async(req,res)=>{
        User.findOneAndUpdate({email:req.body.email},{$set:{isAdmin:true}},{new:true})
        .then(result=>{
            if(result===null){
                res.json({status:"error",error:"email not yet registered"})
            }else{
                res.json(result)
            }
        })
   

}

module.exports.removeAdminAccess=async(req,res)=>{
    User.findOneAndUpdate({email:req.body.email},{$set:{isAdmin:false}},{new:true})
    .then(result=>{
        if(result===null){
            res.json({status:"error",error:"email not yet registered"})
        }else{
            res.json(result)
        }
    })
}

module.exports.deleteUser=async(req,res)=>{
    User.findOneAndDelete({email:req.body.email}).then(result=>{
        if(result===null){
            res.json({status:"error",error:"email not found"})
        }else{
            res.json({status:"ok",deleted:result})
        }
    })
}