const Course = require("../models/Course")

	//Only admin can create a course
module.exports.createCourse= async(req,res)=>{
        let newCourse = new Course({
            courseName:req.body.courseName,
            description: req.body.description,
            price:req.body.price,
            isOffered:req.body.isOffered,
            enrollees:req.body.enrollees
        })
    try{
        await newCourse.save(newCourse).then(result=>res.json({result}))
    }catch(e){
        res.json({status:"error",error:"Duplicate Course Title"})
    }   
}
//Create a route "/" to get all the courses, return all documents found
	//both admin and regular user can access this route
module.exports.getAllCourses=(req,res)=>Course.find().then(result=>res.json(result))


//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
	//both admin and regular user can access this route
module.exports.course=(req,res)=>Course.findById(req.params.id).then(result=>res.json(result))


//Create a route "/:courseId/update" to update course info, return the updated document
	//Only admin can update a course
module.exports.updateCourse=(req,res)=>{
    let updateCourse ={
        courseName:"updated Course",
        description:"new description",
        price:"30000",
        isOffered:true
    }
    Course.findByIdAndUpdate(req.params.id,{$set:updateCourse},{new:true})
    .then(result=>res.json(result))
}


//Create a route "/:courseId/archive" to update isActive to false, return updated document
	//Only admin can update a course

module.exports.archive=(req,res)=> Course.findByIdAndUpdate(req.params.id,{$set:{isOffered:false}},{new:true}).then(result=>res.json(result))

//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
	//Only admin can update a course
    module.exports.unArchive=(req,res)=> Course.findByIdAndUpdate(req.params.id,{$set:{isOffered:true}},{new:true}).then(result=>res.json(result))



//Create a route "/isActive" to get all active courses, return all documents found
	//both admin and regular user can access this route
module.exports.allOfferedCourses=(req,res)=> Course.find({isOffered:true}).then(result=>res.json(result))

//Create a route "/:id/delete-course" to delete a courses, return true if success
	//Only admin can delete a course

 module.exports.deleteCourse=(req,res)=> Course.findByIdAndDelete(req.params.id).then(result=>res.json(result))
