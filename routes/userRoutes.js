const express = require('express');
const router = express.Router();
const {register,getAllUsers,login,profile,update,updatePassword,giveAdminAccess,removeAdminAccess,deleteUser} = require('../controllers/userControllers.js');
const {verifyToken,adminCheck}= require('./../auth')

router.get('/users',(req,res)=>getAllUsers(req,res))

router.post('/users/register',(req,res)=>register(req,res))

router.post('/users/login',(req,res)=>login(req,res))

router.get('/profile',verifyToken,(req,res)=>profile(req,res))

router.put('/update',verifyToken,(req,res)=>update(req,res))

router.put('/update-password',verifyToken,(req,res)=>updatePassword(req,res));

router.patch('/toAdmin',verifyToken,adminCheck,(req,res)=>giveAdminAccess(req,res))

router.patch('/toUser',verifyToken,adminCheck,(req,res)=>removeAdminAccess(req,res))

router.delete('/delete-user',verifyToken,adminCheck,(req,res)=>deleteUser(req,res))

module.exports = router;