const express = require('express');
const router = express.Router();
const {verifyToken,adminCheck}= require('./../auth')
const {createCourse,getAllCourses,course,updateCourse,allOfferedCourses,deleteCourse,archive,unArchive}=require('./../controllers/courseController')

router.post('/create',verifyToken,adminCheck,(req,res)=>createCourse(req,res))

router.get('/',verifyToken,(req,res)=>getAllCourses(req,res))

router.get('/offeredCourses',verifyToken,(req,res)=>allOfferedCourses(req,res))



router.get('/:id',verifyToken,(req,res)=>course(req,res))

router.patch('/:id/update',verifyToken,adminCheck,(req,res)=>updateCourse(req,res));

router.delete('/:id/delete-course',verifyToken,adminCheck,(req,res)=>deleteCourse(req,res));

router.patch('/:id/archive',verifyToken,adminCheck,(req,res)=>archive(req,res));

router.patch('/:id/unArchive',verifyToken,adminCheck,(req,res)=>unArchive(req,res));


module.exports = router;