const cors= require('cors')
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv').config()
const port = process.env.PORT || 3007
const app = express();
app.use(express.json())
app.use(cors());
const userRoutes = require('./routes/userRoutes')

const courseRoutes = require('./routes/courseRoutes')
app.use('/api/courses',courseRoutes)
app.use('/api/',userRoutes)

mongoose.connect(process.env.MONGO)
const db = mongoose.connection;

db.on('open',()=>{
    console.log("connected to database")
})
db.on('error',console.error.bind(console,'connection error:'))

app.listen(port,()=>{
    console.log("Server connect to port",port)
})