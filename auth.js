const res = require('express/lib/response')
const jwt = require('jsonwebtoken')

module.exports.createToken=(body)=>{
    let {id,email,password,isAdmin}=body
    return {token:jwt.sign({id:id,email:email,password:password,isAdmin:isAdmin},process.env.SECRETKEY,{expiresIn:"1h"})}
}
module.exports.verifyToken=(req,res,next)=>{
    const requestToken = req.headers.authorization;
    if(typeof requestToken=="undefined"){
        res.status(401).send("Token Missing")
    }else{
        const token = requestToken.slice(7,requestToken.length)
        jwt.verify(token,process.env.SECRETKEY,(err,data)=>{
            if(err){
                 res.status(505).json({auth:"auth failed!"})
            }else{
                console.log("Token verified, access granted")
                next()
            }
        })
    }
    
}
module.exports.decodeToken=(token)=>{
    return decode(token)
}
module.exports.adminCheck=(req,res,next)=>{
    let token = req.headers.authorization
    let admin = decode(token).isAdmin
    if(admin=="true"){
        next();
    }else{
        res.status(500).json({status:"error",error:"Access Denied, admin required"})
    }
}
function decode(token){
    let bearertoken= token.slice(7,token.length)
    let decoded = jwt.verify(bearertoken,process.env.SECRETKEY)
    return decoded
}