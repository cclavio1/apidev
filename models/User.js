const mongoose = require('mongoose')

const User = new mongoose.Schema({
    firstName:{type:String,required:[true,"First name is required"]},
    lastName:{type:String,required:[true,"Last name is required"]},
    email:{type:String,required:[true,"Email name is required"],unique:true},
    password:{type:String,required:true},
    isAdmin:{Boolean,default:false},
    enrollments:[
        {courseId:{type:String}},
        {status:String,default:"Enrolled"},
        {enrolledOn:Date,default:new Date()}
    ]
},{timestamps:true})

const model = mongoose.model("user",User)

module.exports = model;